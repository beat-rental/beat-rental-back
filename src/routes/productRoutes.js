import express from "express";
import { 
    createProduct,
    deleteProduct,
    getProduct,
    getProducts,
    updateProduct,
    productsNotBooked
} from "../controllers/productController.js";
import { checkAuthAdmin } from "../middlewares/checkAuth.js";

export const productRoutes = express.Router()

productRoutes.get('/all-products', getProducts);

productRoutes.post('/create', checkAuthAdmin, createProduct);

productRoutes.post('/products-not-booked', productsNotBooked);

productRoutes
  .route('/:id')
  .get(getProduct)
  .put(checkAuthAdmin, updateProduct)
  .delete(checkAuthAdmin, deleteProduct);
