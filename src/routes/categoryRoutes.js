import express from "express";
import { createCategory, deleteCategory, getCategories, getCategoryById, updateCategory } from "../controllers/categoryController.js";
import { checkAuthAdmin } from "../middlewares/checkAuth.js";

export const categoryRoutes = express.Router();

categoryRoutes.post('/create', checkAuthAdmin, createCategory);
categoryRoutes.get('/all-categories', getCategories);
categoryRoutes
    .route('/:id')
    .get(getCategoryById)
    .put(checkAuthAdmin, updateCategory)
    .delete(checkAuthAdmin, deleteCategory);
