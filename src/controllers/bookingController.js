import { Booking, Category, Characteristics, Images, Product, User } from "../models/models.js"


export const createBooking = async (req, res) => {
    try {
        let bookingData = req.body;
        const user = await User.findByPk(bookingData.user_id);
        if (!user) {
            return res.status(403).send({ message: 'User not found' });
        }
        const product = await Product.findByPk(bookingData.product_id, { include: ['images', 'category', 'characteristics'] });
        if (!product) {
            return res.status(403).send({ message: 'Product not found' })
        }
        const bookingSave = await Booking.create(bookingData);
        const resp = {
            ...bookingSave.dataValues,
            product,
            user
        }
        res.json(resp)
    } catch (e) {
        console.log(e);
    }
}

export const getBookingsByUser = async (req, res) => {
    const user_id = req.user.id
    const bookings = await Booking.findAll({
        where: { user_id },
        include: [
            {
                model: Product,
                include: [
                    {
                        model: Images,
                    },
                    {
                        model: Characteristics
                    },
                    {
                        model: Category
                    },
                ],
            },
            {
                model: User,
                attributes: [
                    'id', 'first_name', 'last_name', 'email'
                ]
            },
        ]
    });
    res.status(200).json(bookings)
}
export const getBookingsByProduct = async (req, res) => {
    const product_id = req.params.product_id;
    const bookings = await Booking.findAll({
        where: { product_id },
        attributes: {
            exclude: ['price', 'user_id']
        },
        // include: [
        //     {
        //         model: Product,
        //         include: [
        //             {
        //                 model: Images
        //             },
        //             {
        //                 model: Characteristics
        //             },
        //             {
        //                 model: Category
        //             },
        //         ],
        //     },
        //     {
        //         model: User,
        //         attributes: [
        //             'id', 'first_name', 'last_name', 'email'
        //         ]
        //     },
        // ],
        
    });
    res.status(200).json(bookings)
}
export const getBookingById = async (req, res) => {
    const bookingId = req.params.id;
    const booking = await Booking.findByPk(bookingId, {
        include: [
            {
                model: Product,
                include: [
                    {
                        model: Images,
                    },
                    {
                        model: Characteristics
                    },
                    {
                        model: Category
                    },
                ],
            },
            {
                model: User,
                attributes: [
                    'id', 'first_name', 'last_name', 'email'
                ]
            },
        ]
    });
    if (!booking) {
        return res.status(404).send({ msg: 'Booking not found' });
    }
    return res.status(200).json({
        success: true,
        data: booking,
        msg: 'Booking found'
    });
}
export const updateBooking = async (req, res) => {
    const bookingId = req.params.id;
    const data = req.body;
    try {
        let bookingToUpdate = await Booking.findByPk(bookingId);
        if (!bookingToUpdate) {
            return res.status(404).send({ msg: "Booking not found" })
        }
        let booking = await Booking.update(data, { where: { id: bookingId } });
        console.log(booking[0])
        if (!booking[0]) {
            throw new Error("Error updating");
        }
        return res.status(200).json({
            success: true,
            msg: "Successfully updated resource"
        });
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Server error" })
    }
}
export const deleteBooking = async (req, res) => {
    const bookingId = req.params.id;
    try {
        let bookingToDelete = await Booking.findByPk(bookingId);
        if(!bookingToDelete){
            return res.status(404).send({msg:"Booking not found"})
        }
        let deletedRows = await Booking.destroy({ where: { id: bookingId } })
        if (!deletedRows) {
            throw new Error("Error deleting")
        }
        return res.status(200).json({ message: 'Resource successfully eliminated' })
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Server error" })
    }
}