import { JWT_SECRET } from "../config/dotenv.js";
import { User } from "../models/models.js";
import jwt from "jsonwebtoken";

export const checkAuthAdmin = async (req, res, next) => {
    let token;
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        try {
            token = req.headers.authorization.split(' ').pop()
            const decoded = jwt.verify(token, JWT_SECRET)
            const { id, type_user } = decoded;
            const user = await User.findOne({
                where: {
                    id
                },
                attributes: { exclude: ['password', 'type_user'] }
            });

            if (!user) {
                return res.status(401).json({ msg: 'Not valid user' });
            }

            if(type_user === "CLIENT"){
                return res.status(401).json({ msg: 'The user does not have permissions' });
            }

            req.user = user;
            return next()
        } catch (error) {
            console.error(error);
            res.status(500).json({ msg: error });
        }
    }

    if(!token){
        return res.status(401).json({msg:"No token provided"})
    }
}

export const checkAuthClient = async (req, res, next) => {
    let token;
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        try {
            token = req.headers.authorization.split(' ').pop()
            const decoded = jwt.verify(token, JWT_SECRET)
            const { id } = decoded;
            const user = await User.findOne({
                where: {
                    id
                },
                attributes: { exclude: ['password', 'type_client'] }
            });

            if (!user) {
                return res.status(401).json({ msg: 'Not valid user' });
            }

            req.user = user;
            return next()
        } catch (error) {
            console.error(error);
            res.status(500).json({ msg: error });
        }
    }

    if(!token){
        return res.status(401).json({msg:"No token provided"})
    }
}