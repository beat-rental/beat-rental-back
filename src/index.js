import './config/dotenv.js'
import express from "express";
import fileUpload from "express-fileupload";
import cors from 'cors';
import { connectDB } from "./config/db_config.js";
import { imagesRoutes } from "./routes/imagesRoutes.js";
import { productRoutes } from "./routes/productRoutes.js";
import { userRoutes } from "./routes/userRoutes.js";
import { categoryRoutes } from './routes/categoryRoutes.js';
import { PORT_SERVER, FRONTEND_URL } from "./config/dotenv.js";
import { characteristicsRoutes } from './routes/characteristicsRoutes.js';
import { bookingRoutes } from './routes/bookingRoutes.js';
const app = express();

// define port
const PORT = PORT_SERVER || 8080;

// allowed urls
const URLS = [
  FRONTEND_URL,
  'http://192.168.56.1:8080/',
  'http://192.168.1.4:8080/',
  'http://localhost:5173',
  'http://localhost:5174',
  'http://localhost:3000',
  'http://localhost:8080',
];

// config CORS
const corsOptions = {
  origin: URLS,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true, // Habilita el envío de cookies y encabezados de autorización
};

// middleware
app.use(fileUpload({
  useTempFiles: true,
  tempFileDir: './uploads-images'
}));
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// routes
app.use("/api/v1/product", productRoutes);
app.use("/api/v1/user", userRoutes);
app.use("/api/v1/images", imagesRoutes);
app.use("/api/v1/category", categoryRoutes);
app.use("/api/v1/characteristics", characteristicsRoutes);
app.use("/api/v1/booking", bookingRoutes);

// start server
app.listen(PORT, () => console.log(`API listening on port ${PORT}`))

//DB connection
connectDB();