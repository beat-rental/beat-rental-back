import { 
    AWS_ACCESS_KEY,
    AWS_BUCKET_NAME,
    AWS_BUCKET_REGION,
    AWS_SECRET_KEY,
    DOMAIN_BUCKET_S3
} from "../config/dotenv.js";
import { S3Client, PutObjectCommand } from "@aws-sdk/client-s3";
import fs from "fs";

const client = new S3Client({
    region: AWS_BUCKET_REGION,
    credentials: { 
        accessKeyId: AWS_ACCESS_KEY, 
        secretAccessKey: AWS_SECRET_KEY 
    },

})

export const saveImage = async (image) => {
    const stream = fs.createReadStream(image.tempFilePath);

    const name = image.name
    .replaceAll('  ', ' ')
    .replaceAll('  ', ' ')
    .replaceAll('  ', ' ')
    .replaceAll(' ', '-');

    const params = {
        Bucket: AWS_BUCKET_NAME,
        Key: `uploads-images/${Date.now()}-${name}`,
        Body: stream,
    }

    const command = new PutObjectCommand(params);
    const response = await client.send(command);
    response.urlToSave = `${DOMAIN_BUCKET_S3}/${params.Key}`;
    response.name = params.Key;
    return response;
}