import { Booking, Category, Characteristics, Images, Product } from "../models/models.js";
import moment from "moment/moment.js";

export const createProduct = async (req, res) => {
    try {
        let newProduct = await Product.create(req.body);
        return res.status(200).json({
            success: true,
            data: newProduct,
            msg: "New product created"
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({
            error: err
        })
    };

}

export const getProducts = async (req, res) => {
    const products = await Product.findAll({ include: ['images', 'category', 'characteristics'] });
    return res.status(200).json(products)
}

export const getProduct = async (req, res) => {
    const productId = req.params.id;
    const product = await Product.findByPk(productId, { include: ['images', 'category', 'characteristics'] });
    if (!product) {
        return res.status(404).send({ msg: 'Product not found' });
    }
    return res.status(200).json({
        success: true,
        data: product,
        msg: 'Product found'
    });

}

export const updateProduct = async (req, res) => {
    const productId = req.params.id;
    const data = req.body;
    try {
        let product = await Product.update(data, { where: { id: productId } });
        if (!product[0]) {
            throw new Error("Error updating");
        }
        return res.status(200).json({
            success: true,
            data: product[0],
            msg: "Successfully updated resource"
        });
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Server error" })
    }
}

export const deleteProduct = async (req, res) => {
    const productId = req.params.id;
    try {
        let deletedRows = await Product.destroy({ where: { id: productId } })
        if (!deletedRows) {
            throw new Error("Error deleting")
        }
        return res.status(200).json({ message: 'Resource successfully eliminated' })
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Server error" })
    }
}


export const productsNotBooked = async (req, res) => {
    try {
        const { initialDate, finalDate } = req.body;

        if (!initialDate || !finalDate) {
            return res.status(400).json({ error: 'Initial and final date are required' });
        }

        const products = await Product.findAll({
            include: [
                {
                    model: Booking
                },
                {
                    model: Images
                },
                {
                    model: Characteristics
                },
                {
                    model: Category
                },
            ]
        });

        const productsNotBooked = products.map(product => {
            const bookings = product.bookings.filter(booking => {
                const startHour = moment(booking.initial_date);
                const endHour = moment(booking.final_date);
                if ((moment(initialDate) <= startHour && moment(finalDate) >= startHour) || (moment(initialDate) <= endHour && moment(finalDate) >= endHour)) {
                    return booking
                }
            })
            if (bookings.length === 0) {
                return {
                    id: product.id,
                    name: product.name,
                    description: product.description,
                    price: product.price,
                    creation_date: product.creation_date,
                    category_id: product.category_id,
                    images: product.images,
                    characteristics: product.characteristics,
                    category: product.category
                }
            } else {
                return false
            }
        }).filter(product => product != false)

        if (productsNotBooked.length > 0) {
            return res.status(200).json(productsNotBooked);
        } else {
            return res.status(400).json({ message: 'There are no products available for those dates' });
        }
    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: 'Error interno del servidor' });
    }
}

