
import { Images } from "../models/models.js";
import { saveImage } from "../utils/s3_manager.js";


export const createImages = async (req, res) => {
    const filesSaved = [];
    if (req.files?.images.name) {
        const response = await saveImage(req.files.images);
        filesSaved.push({url: response.urlToSave, name: response.name});
    }else if(req.files?.images[0]){
        const dataPromise = req.files.images.map( image => saveImage(image));
        const response = await Promise.all(dataPromise);
        response.forEach( item => {
            filesSaved.push({url: item.urlToSave, name: item.name});
        });
    }else{
        return res.status(400).json({msg: 'Images were not sent correctly'})
    }

    const resp = await Promise.all( filesSaved.map( async fileSaved => {
        let newImage = await Images.create({
            url: fileSaved.url,
            name: fileSaved.name,
            product_id: req.body.product_id
        })
        return newImage;
    } ))
    
    return res.status(200).json({msg: 'Files was received correctly', images: resp})
}

export const getImagesByProduct = async (req, res) => {
    const images = await Images.findAll({
        attributes: [
            'url'
        ],
        where:{
            product_id: req.params.id
        },
    });

    if (images.length < 1) {
        return res.status(400).json({msg: `No product images with id ${req.params.id}`, })
    }else{
        return res.status(200).json({images});
    }
}

export const getImages = async (req, res) => {
    const images = await Images.findAll();
    return res.status(200).json(images)
}