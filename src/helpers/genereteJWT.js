import { JWT_SECRET } from "../config/dotenv.js";
import jwt from "jsonwebtoken";

export const generateJWT = (id, type_user) => {
  return jwt.sign({ id, type_user }, JWT_SECRET, {
    expiresIn: '30d'
  })
}