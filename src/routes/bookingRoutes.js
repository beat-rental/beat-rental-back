import express from "express";
import { createBooking, getBookingsByUser, getBookingById, updateBooking, deleteBooking, getBookingsByProduct } from "../controllers/bookingController.js";
import { checkAuthClient } from "../middlewares/checkAuth.js";

export const bookingRoutes = express.Router();

bookingRoutes.post('/create', checkAuthClient, createBooking);
bookingRoutes.get('/all-bookings', checkAuthClient, getBookingsByUser);
bookingRoutes.get('/all-bookings/:product_id', checkAuthClient, getBookingsByProduct);
bookingRoutes
    .route('/:id')
    .get(checkAuthClient, getBookingById)
    .put(checkAuthClient, updateBooking)
    .delete(checkAuthClient, deleteBooking);
