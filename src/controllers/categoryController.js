import { Category } from "../models/models.js";

export const createCategory = async (req, res) => {

    try {
        const category = await Category.create(req.body);
        return res.status(200).json({
            success: true,
            data: category,
            msg: "Category created correctly"
        });
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Server error" })
    };
}

export const getCategories = async (req, res) => {
    const categories = await Category.findAll();
    return res.status(200).json(categories);
}

export const getCategoryById = async (req, res) => {
    const category = await Category.findByPk(req.params.id);

    if (!category) {
        return res.status(404).send({ msg: 'Category not found' });
    }
    return res.status(200).json(category);
};

export const updateCategory = async (req, res) => {
    try {
        const category = await Category.update(req.body, { where: { id: req.params.id } });
        if (!category[0]) {
            throw new Error("Error updating");
        }
        return res.status(200).json({
            success: true,
            data: category[0],
            msg: "Successfully updated resource"
        });
    } catch (error) {
        console.log(err);
        return res.status(500).send({ message: "Server error" })
    }
}; 

export const deleteCategory = async (req, res) => {
    try {
        const deletedCategory = await Category.destroy({ where: { id: req.params.id } })
        if (!deletedCategory) {
            throw new Error("Error deleting")
        }
        return res.status(200).json({ message: 'Resource successfully eliminated' })
    } catch (err) {
        console.log(err);
        return res.status(500).send({ message: "Server error" })
    }
}