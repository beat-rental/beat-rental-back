import express from "express";
import { createImages, getImagesByProduct, getImages } from "../controllers/imagesController.js";
import { checkAuthAdmin } from "../middlewares/checkAuth.js";

export const imagesRoutes = express.Router();

imagesRoutes.post('/create', checkAuthAdmin, createImages);

imagesRoutes.get('/product_id/:id', getImagesByProduct);

imagesRoutes.get('/all-images', getImages);
