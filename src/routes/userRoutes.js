import express from "express";
import { login, register } from "../controllers/userController.js";

export const userRoutes = express.Router()

userRoutes.post('/register', register);
userRoutes.post('/login', login); // Nueva ruta para el inicio de sesión