import { Sequelize } from "sequelize";
import { 
  NAME_DATABASE,
  USER_DATABASE,
  PASSWORD_DATABASE,
  ENDPOINT_DATABASE,
  // ENDPOINT_DATABASE_DEV,
  // USER_DATABASE_DEV,  
  // NAME_DATABASE_DEV,  
  // PASSWORD_DATABASE_DEV
} from "../config/dotenv.js";

export const sequelize_config = new Sequelize(
  NAME_DATABASE, 
  USER_DATABASE, 
  PASSWORD_DATABASE, 
  // NAME_DATABASE_DEV, 
  // USER_DATABASE_DEV, 
  // PASSWORD_DATABASE_DEV, 
  {
    host: ENDPOINT_DATABASE,
    // host: ENDPOINT_DATABASE_DEV,
    dialect: 'mysql',
    pool: {
      acquire: 30000,
      idle: 10000
    }
  }
);

export const connectDB = async () => {

  try {
    await sequelize_config.authenticate();
    await sequelize_config.sync();
    console.log('Connection to the database has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
    throw new Error(error)
  }
}