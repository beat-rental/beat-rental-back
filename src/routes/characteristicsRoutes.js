import express from "express";
import { createCharacteristics, getCharacteristicsByProduct, getCharacteristics } from "../controllers/characteristicsController.js";
import { checkAuthAdmin } from "../middlewares/checkAuth.js";

export const characteristicsRoutes = express.Router();

characteristicsRoutes.post('/create', checkAuthAdmin, createCharacteristics);

characteristicsRoutes.get('/product_id/:id', getCharacteristicsByProduct);

characteristicsRoutes.get('/all-characteristics', getCharacteristics);
