import { User } from "../models/models.js";
import bcrypt from "bcrypt";
import { generateJWT } from "../helpers/genereteJWT.js";

export const register = async (req, res) => {
  // Check if user already exists
  const { email } = req.body;
  const existingUser = await User.findOne({
    where: {
      email
    }
  })
  if (existingUser) {
    const error = new Error("The user already exists")
    return res.status(404).json({ msg: error.message })
  }

  try {
    const user = await User.create(req.body)
    await user.save()

    return res.json({ 
      success: true,
      data: user,
      msg: "The user has been created correctly" 
    })
  } catch (error) {
    console.log(error)
  }
}


export const login = async (req, res) => {
  try {
    const { email, password } = req.body;

    // Verificar las credenciales en la base de datos
    const user = await User.findOne({
      where: {
        email
      }
    });

    if(!user){
      const error = new Error("The user does not exist");
      return res.status(404).json({msg: error.message}) 
    }

    if (!(await bcrypt.compare(password, user.password))) {
      const error = new Error("Incorrect credentials");
      return res.status(401).json({msg: error.message});
    }

    if(user.token){
      return res.status(200).json({
        success: true,
        data: {
          user
        },
        msg: 'Successful login'
      });
    }

    // Generar token de autenticación
    const token = generateJWT(user.id, user.type_user)
    user.token = token;
    await user.save();

    return res.status(200).json({
      success: true,
      data: {
        user
      },
      msg: 'Successful login'
    });
  } catch (error) {
    console.error('Error logging in:', error);
    return res.status(500).json({ msg: 'Server error when logging in' });
  }
};