
import { Characteristics } from "../models/models.js";

export const createCharacteristics = async (req, res) => {
    try {
        const { characteristics, product_id } = req.body;
        const result = [];
        const dataPromise = characteristics.map(async characteristic => await Characteristics.create({characteristic, product_id}) )
        const response = await Promise.all(dataPromise);
        response.forEach( item => {
            result.push(item);
        });
        return res.status(200).json({msg: 'Characteristics created correctly', characteristics: result})
    } catch(error) {
        console.log(error)
        return res.status(500).json({msg: 'Error creating characteristics'})
    }
}

export const getCharacteristicsByProduct = async (req, res) => {
    const characteristics = await Characteristics.findAll({
        attributes: [
            'characteristic'
        ],
        where:{
            product_id: req.params.id
        },
    });

    if (characteristics.length < 1) {
        return res.status(400).json({msg: `There are no characteristics for the product with id ${req.params.id}`, })
    }else{
        return res.status(200).json({characteristics});
    }
}

export const getCharacteristics = async (req, res) => {
    const characteristics = await Characteristics.findAll();
    return res.status(200).json(characteristics)
}
