import { DataTypes } from "sequelize";
import { sequelize_config } from "../config/db_config.js";
import bcrypt from "bcrypt";

export const Category = sequelize_config.define("categories", {
    id:{
        type: DataTypes.UUID,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUIDV4
    },
    category_name: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false,
    freezeTableName: true
});

export const Images = sequelize_config.define("images", {
    id:{
        type: DataTypes.UUID,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUIDV4
    },
    url: {
        type: DataTypes.STRING,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false,
    freezeTableName: true
});

export const Product = sequelize_config.define("products", {
    id:{
        type: DataTypes.UUID,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUIDV4
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.TEXT
    },
    price: {
        type: DataTypes.FLOAT,
        allowNull: false
    },
    creation_date: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW
    }
}, {
    timestamps: false,
    freezeTableName: true
});

export const User = sequelize_config.define("users", {
    id:{
        type: DataTypes.UUID,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUIDV4
    },
    first_name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    last_name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    type_user: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: 'CLIENT'
    },
    token: {
        type: DataTypes.STRING,
        defaultValue: null
    }
    
    
}, {
    timestamps: false,
    freezeTableName: true
});

export const Characteristics = sequelize_config.define("characteristics", {
    id:{
        type: DataTypes.UUID,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUIDV4
    },
    characteristic: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false,
    freezeTableName: true
});

export const Booking = sequelize_config.define("bookings", {
    id:{
        type: DataTypes.UUID,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUIDV4
    },
    initial_date: {
        type: DataTypes.DATE,
        allowNull: false
    },
    final_date: {
        type: DataTypes.DATE,
        allowNull: false
    },
    price: {
        type: DataTypes.FLOAT,
        allowNull: false
    }
}, {
    timestamps: false,
    freezeTableName: true
});

// Middleaware
User.beforeCreate(async (user) => {
    const saltRounds = 10;
    user.password = await bcrypt.hash(user.password, saltRounds);
  });

// Relationships between tables
Product.hasMany(Images, { foreignKey: 'product_id', onDelete: 'CASCADE'});
Images.belongsTo(Product, { foreignKey: 'product_id'});

Product.hasMany(Characteristics, { foreignKey: 'product_id', onDelete: 'CASCADE'});
Characteristics.belongsTo(Product, { foreignKey: 'product_id'});

Category.hasMany(Product, { foreignKey: 'category_id' });
Product.belongsTo(Category, { foreignKey: 'category_id' });

Product.hasMany(Booking, { foreignKey: 'product_id', onDelete: 'CASCADE'});
User.hasMany(Booking, { foreignKey: 'user_id'});

Booking.belongsTo(Product, { foreignKey: 'product_id'});
Booking.belongsTo(User, { foreignKey: 'user_id'});

