import { config } from "dotenv";

config()

export const ENDPOINT_DATABASE = process.env.ENDPOINT_DATABASE;
export const USER_DATABASE = process.env.USER_DATABASE;
export const NAME_DATABASE = process.env.NAME_DATABASE;
export const PASSWORD_DATABASE = process.env.PASSWORD_DATABASE;
export const FRONTEND_URL = process.env.FRONTEND_URL;
export const PORT_SERVER = process.env.PORT_SERVER;
export const AWS_BUCKET_NAME = process.env.AWS_BUCKET_NAME;
export const AWS_BUCKET_REGION = process.env.AWS_BUCKET_REGION;
export const AWS_ACCESS_KEY = process.env.AWS_ACCESS_KEY;
export const AWS_SECRET_KEY = process.env.AWS_SECRET_KEY;
export const DOMAIN_BUCKET_S3 = process.env.DOMAIN_BUCKET_S3;
export const JWT_SECRET = process.env.JWT_SECRET;

export const ENDPOINT_DATABASE_DEV = process.env.ENDPOINT_DATABASE_DEV;
export const USER_DATABASE_DEV = process.env.USER_DATABASE_DEV;
export const NAME_DATABASE_DEV = process.env.NAME_DATABASE_DEV;
export const PASSWORD_DATABASE_DEV = process.env.PASSWORD_DATABASE_DEV;